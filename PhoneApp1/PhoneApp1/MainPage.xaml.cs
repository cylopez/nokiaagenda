﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        private const string ConnectionString = @"isostore:/agenda.sdf";

        // Constructor
        public MainPage()
        {
            InitializeComponent();
          
            //Hay base? si-> conectate, no-> creala
            using (AgendaContext context = new AgendaContext(ConnectionString))
            {

                if (!context.DatabaseExists())
                {
                    // create database if it does not exist
                    context.CreateDatabase();
                }
            }  //Tu base existe
            
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textBox1.Text == "Buscar")
                SeleccionaContactos();
            else
            FiltroContacto(textBox1.Text);
        }
        private void textBox1_GotFocus(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text == "Buscar")
              {
            textBox1.Text = "";
            SolidColorBrush Brush1 = new SolidColorBrush();
            Brush1.Color = Colors.Magenta;
            textBox1.Foreground = Brush1;
            
              }
        }

            private void textBox1_LostFocus(object sender, RoutedEventArgs e)
            {
               if (textBox1 .Text == String.Empty)
                {
                textBox1.Text = "Buscar";
                SolidColorBrush Brush2 = new SolidColorBrush();
                Brush2.Color = Colors.Gray;
                textBox1.Foreground = Brush2;
                
                }
            }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {

            listBox1.Items.Clear();


            SeleccionaContactos();
        }

        
        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        
            
        {
            if (listBox1.SelectedIndex != -1)
            {
                if ((MessageBox.Show(((TextBlock)(listBox1.SelectedValue)).Text, " Modificar Contacto", MessageBoxButton.OKCancel)) == MessageBoxResult.OK)
                    NavigationService.Navigate(new Uri("/Page1.xaml?msg=" + ((TextBlock)(listBox1.SelectedValue)).Text, UriKind.RelativeOrAbsolute));
            }
          
               listBox1.SelectedIndex = -1;
            


        

        }

        private void bAgregar_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page1.xaml?msg=" + "", UriKind.RelativeOrAbsolute));
           

        }


        private void LlenaLista(IList<Agenda1>lista)
        {
            listBox1.Items.Clear();
          
            
            /*
             * 
             * foreach
             * es como si fuera
             * 
             * Agenda1 agenda=new Agenda();
             * IList<Agenda1> lista=SeleccionaContactos();
             * for(int i=0;i<lista.size();i++)
             * {
             *  agenda=lista.get(i);
             *  contacto.Text=agenda.Nombre;
             *  listBox1.Items.Add(contacto);
             * }
             * 
             */

            foreach(Agenda1 agenda in lista)
            {
               
                TextBlock contacto = new TextBlock();
                contacto.Text = agenda.Nombre+"\n"+agenda.Celular;


                listBox1.Items.Add(contacto);
            }
        }
       private void FiltroContacto(string nombre)
        {
            try
            {
                IList<Agenda1> contactosLista = null;

                using (AgendaContext context = new AgendaContext(ConnectionString))
                {
                    //Esto es equivalente a un like
                    /*
                     * La primera vez nombre=I
                     * la segunda nombre=IV
                     */
                    IQueryable<Agenda1> AgendaSelect = from a in context.Agendas where a.Nombre.StartsWith(nombre) select a;

                    contactosLista = AgendaSelect.ToList();
                    
                    //Seleccion del elemento unico


                    //Llenado de la lista con el filtro
                    LlenaLista(contactosLista);

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Selecciona Todos
        private void SeleccionaContactos()


        {
            IList<Agenda1> contactosLista = null;
            try
            {

               
                using (AgendaContext context = new AgendaContext(ConnectionString))
                {
                    IQueryable<Agenda1> query = from c in context.Agendas select c;
                    contactosLista = query.ToList();
                    LlenaLista(contactosLista);
                }
            }


            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
           
        }
        
    }
}