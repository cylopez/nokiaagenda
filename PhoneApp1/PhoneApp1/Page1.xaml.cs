﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Devices;
using System.Globalization;
using Microsoft.Xna.Framework.Media;

namespace PhoneApp1
{
    
    public partial class Page1 : PhoneApplicationPage
    {
        private bool insertar = false;
        private int photoCounter = 0;

        private string nombreTemp;
        PhotoCamera cam;
        MediaLibrary libery = new MediaLibrary();

        
        //Codigo BaseD


        private const string ConnectionString = @"isostore:/agenda.sdf";
 
        public Page1()
        {
            InitializeComponent();

           
            
            using (AgendaContext context = new AgendaContext(ConnectionString))
            {

                if (!context.DatabaseExists())
                {
                    // create database if it does not exist
                    context.CreateDatabase();
                }
            }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            
            base.OnNavigatedTo(e);
            string msg = "";
            NavigationContext.QueryString.TryGetValue("msg", out msg);
             if (msg!="")
                {
                    insertar = false;
                    nombreTemp = msg.Substring(0, msg.IndexOf("\n"));
                    //para actualizar seleccionar el contacto deseado y mostrorlo al usuario para su edicion 
                    SeleccionContacto(msg);
                   

                }
                else
                    insertar = true;
            

            InicializarComponentes(insertar);
            

            
            
            
        }
        //limpia o llena caja. aparece o desaparece botones
        private void InicializarComponentes(bool valor)
        {
            if (valor)
            {
                bEliminar.Visibility = Visibility.Collapsed;
                
            }
            else
            {
                //BASE DATOS
                bEliminar.Visibility = Visibility.Visible;

            }

        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void bEliminar_Click(object sender, RoutedEventArgs e)
        {
            EliminarContacto();
            

        }

        private void bGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (tCelular.Text != "")
            {
                if (insertar)
                    insertaContacto();
                else
                {

                    actualizarAgenda();
                }
            }
            else
            {
                MessageBox.Show("Es Obligatorio Numero Celular");
            }

           }

       
        


        //Insertar base de datos

        public void insertaContacto()
        {
            using (AgendaContext context = new AgendaContext(ConnectionString))
            {
                // crea un nuevo registro en  tabla 
                try
                {
                    Agenda1 contacto = new Agenda1();
                    contacto.Nombre = tNombre.Text;
                    contacto.Apellido = tApellido.Text;
                    contacto.Celular = tCelular.Text;
                    contacto.Fijo = tFijo.Text;
                    string fech = (tFcumple.Text!="" ? tFcumple.Text:"01/01/2000") + " 01:00:00 AM";
                    contacto.FechNacimiento = DateTime.ParseExact(fech, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    //Ingresar a la base de datos
                    context.Agendas.InsertOnSubmit(contacto);
                    // Guarda los cambios en la base
                    context.SubmitChanges();
                    MessageBox.Show("Se agrego contacto exitosamente");
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se puede insertar "+ex.Message);
                }



                

            }
        }



        //Actualiza
        private void actualizarAgenda()
        {
            using (AgendaContext context = new AgendaContext(ConnectionString))
            {
               
                IQueryable<Agenda1> AgendaSelect = from a in context.Agendas where a.Nombre == nombreTemp select a;
                Agenda1 updateAgenda = AgendaSelect.FirstOrDefault();
                
               
                try
                {

                    updateAgenda.Nombre = tNombre.Text;
                    updateAgenda.Apellido = tApellido.Text;
                    updateAgenda.Celular = tCelular.Text;
                    updateAgenda.Fijo = tFijo.Text;

                    updateAgenda.FechNacimiento = DateTime.Parse(tFcumple.Text);
                  
                    // Guarda los cambios en la base
                    context.SubmitChanges();
                    MessageBox.Show("Se actualizo exitosamente");
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se puede actualizar " + ex.Message);
                }


            }
        }

        //Borra


        private void EliminarContacto()
        {
            using (AgendaContext context = new AgendaContext(ConnectionString))
            {
             
                IQueryable<Agenda1> seleccionContacto = from a in context.Agendas where a.Nombre == tNombre.Text select a;
                Agenda1 agendaToDelete = seleccionContacto.FirstOrDefault();


                context.Agendas.DeleteOnSubmit(agendaToDelete);

              
                context.SubmitChanges();
                MessageBox.Show("Se elimino contacto");
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
            }
        }





       //selecciona un registro

        private void SeleccionContacto(string nombre)
        {
            try
            {
                using (AgendaContext context = new AgendaContext(ConnectionString))
                {

                    
                    IQueryable<Agenda1> AgendaSelect = from a in context.Agendas where a.Nombre == nombreTemp select a;
                    Agenda1 contacto = AgendaSelect.FirstOrDefault();

                    tNombre.Text = contacto.Nombre;
                    tApellido.Text = contacto.Apellido;
                    tFijo.Text = contacto.Fijo;
                    tCelular.Text = contacto.Celular;
                    tFcumple.Text = contacto.FechNacimiento.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}